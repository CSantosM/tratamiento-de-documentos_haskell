module Acronimos(buscarAcronimo, mostrarTituloAcrRevis, listadoAcronimos, listadoAcronimosContador, mostrarSinAcronimo) where
import Articulos
import HerramientasUtiles (listaPorPalabras, isUpperAll, devolverContenido)
import Data.Char
import Data.List


type Acronimo = String
---------------------------------------- Enunciado 3 ----------------------------------------
buscarAcronimo :: [Articulo] -> Acronimo -> Titulo
buscarAcronimo [] _ = ""
buscarAcronimo _ [] = ""
buscarAcronimo (x:xs) acr = (buscarAcronimoAux x acr)++(buscarAcronimo xs acr)

buscarAcronimoAux :: Articulo -> Acronimo -> Titulo
buscarAcronimoAux (CArticulo "" "" "" "" [""] crarInfoVacio) _ = ""
buscarAcronimoAux (CArticulo _ _ _ titulo cont _) acr = if (existeAcronimo (listaPorPalabras cont) acr) then ("Titulo: "++titulo ++ "\n") else ""

existeAcronimo :: Contenido -> Acronimo -> Bool
existeAcronimo cont acr = ((elem ("(" ++ acr ++ ")") (cont)) || (elem ("(" ++ acr ++ "),") (cont)))

---------------------------------------- Enunciado 4 ----------------------------------------
--Si coincide la revista y si el acronimo est� en el contenido, devuelvo el titulo
mostrarTituloAcrRevis :: [Articulo] -> Acronimo -> Revista -> IO ()
mostrarTituloAcrRevis [] _ _ = putStrLn ""
mostrarTituloAcrRevis ((CArticulo r _ _ t cont _):xs) acr revis =do
	if (r == revis) then
		if (existeAcronimo cont acr) then do
			putStrLn ("Titulo: "++ t ++ "\n")
			mostrarTituloAcrRevis xs acr revis
			
		else (mostrarTituloAcrRevis xs acr revis)

	else (mostrarTituloAcrRevis xs acr revis)




---------------------------------------- Enunciado 5 ----------------------------------------
listadoAcronimos :: [Articulo] -> Year -> IO ()
listadoAcronimos [] _ = return ()
listadoAcronimos _ [] = putStr ("No hay anio")
listadoAcronimos ((CArticulo _ _ y _ c _):xs) anio = if (y==anio) then do
													 putStrLn (devolverContenido(listadoAcronimosAux (listaPorPalabras c)(listaPorPalabras c)))
													 listadoAcronimos xs anio
													else listadoAcronimos xs anio



listadoAcronimosAux :: Contenido -> Contenido -> [Acronimo]
listadoAcronimosAux [] _ = []
listadoAcronimosAux (x:xs) copia = do
							 	
							 	
								if ((esAcronimo x) && (acotarAcronimo x)) then do
														
														
									let candidatos = take (contarElementos copia x) copia
									let longitud = length candidatos
									let posicion = longitud - ((length x)-1)
									let expandida = acotarExpandida (drop posicion (candidatos)) x
									nub(expandida ++([x ++ "\n"] )++ (listadoAcronimosAux xs copia))
														
								else
									listadoAcronimosAux xs copia
															


esAcronimo :: Acronimo -> Bool
esAcronimo [] = False
esAcronimo (x:xs) = ((x=='(') || (x==')')) && isUpperAll xs

acotarAcronimo :: Acronimo -> Bool
acotarAcronimo acr = ((length acr > 3) && ((last acr ==')') || (last acr =='.') || (last acr ==',')) && (not (isDigit (last(take 2 acr)))))

acotarExpandida :: Contenido -> Acronimo -> Contenido
acotarExpandida [] _ = []
acotarExpandida (x:xs) acr = do
								let acrSinParentesis = init (drop 1 acr)
								if(guardaRelacion x acrSinParentesis) then do
																		
									
									([x]++ (acotarExpandida xs acr))
									
								 else (acotarExpandida xs acr)

guardaRelacion :: String -> Acronimo -> Bool
guardaRelacion [] [] = False
guardaRelacion _ [] = True
guardaRelacion [] _ = False
guardaRelacion (x:xs) (a:as) = if (((x==a) || (guardaRelacion [x] as)) || guardaRelacion xs as) then True else False

contarElementos :: Contenido -> Acronimo -> Int
contarElementos [] _ = 0
contarElementos (x:xs) acr = if x==acr then 0 else 1+contarElementos xs acr 								
				
---------------------------------------- Enunciado 6 ----------------------------------------
listadoAcronimosContador :: [Articulo] -> ID -> IO()
listadoAcronimosContador [] _ = return()
listadoAcronimosContador ((CArticulo _ i _ _ c _):xs) ident = if (i == ident) then 
																putStrLn(devolverContenido (nub(listadoAcronimosContadorAux (listaPorPalabras c) (listaPorPalabras c))))
															else
																listadoAcronimosContador xs ident


listadoAcronimosContadorAux :: Contenido -> Contenido -> [String]
listadoAcronimosContadorAux [] _ = []
listadoAcronimosContadorAux (x:xs) copia = if ((esAcronimo x) && (acotarAcronimo x)) then do
											let acr = init (drop 1 x) --quito parentesis
											let nveces = (contarApariciones acr copia)+1
											["El acronimo : " ++ x ++ " Aparece " ++ show nveces ++ " veces \n"]++(listadoAcronimosContadorAux xs copia)
											
										else
											listadoAcronimosContadorAux xs copia
											
											

contarApariciones :: Acronimo -> Contenido -> Int
contarApariciones [] _ = 0
contarApariciones acr cont = length [x | x <- cont, x == acr]
											
										
																

---------------------------------------- Enunciado 7 ----------------------------------------

mostrarSinAcronimo :: [Articulo] -> IO ()
mostrarSinAcronimo [] = return ()
mostrarSinAcronimo ((CArticulo _ i _ "" c _):as) = return ()
mostrarSinAcronimo ((CArticulo _ i _ t c _):as) = if not(mostrarSinAcronimoAux (listaPorPalabras c)) then do
														
													putStrLn("Titulo: " ++ t ++" (" ++ i ++ ") \n")
													(mostrarSinAcronimo as)
													
													 
												else mostrarSinAcronimo as



mostrarSinAcronimoAux :: Contenido -> Bool
mostrarSinAcronimoAux [] = False
mostrarSinAcronimoAux (x:xs) = (esAcronimo x) || (mostrarSinAcronimoAux xs)
----------------------------------------------------------------------------------------------




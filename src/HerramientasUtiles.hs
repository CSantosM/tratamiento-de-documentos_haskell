module HerramientasUtiles where
import Data.Char
 
 

separarLineas :: String -> [String]
separarLineas cadena = lines cadena


-- Id�ntica a la funcion "lines" de Haskell pero crea un nuevo elemnto de una lista
-- por cada espacio encontrado y no por cada salto de linea
--- usada para saber si un Acronimo pertenece al contenido con la funcion "elem" 
linesPorEspacio :: String -> [String]
linesPorEspacio "" = []
linesPorEspacio s =  cons (case break (== ' ') s of
				(l, s') -> (l, case s' of
	                            	[] -> []
	                                _:s'' -> linesPorEspacio s''))
	where
		cons ~(h, t)        =  h : t



-------------------------- Convierto el contenido en una lista de palabras
listaPorPalabras :: [String] -> [String]
listaPorPalabras cont = linesPorEspacio (devolverContenido cont)

-------------------------- Uso la funcion quicksort para ordenar los art�culos
ordenar :: Ord a => [a] -> [a]
ordenar [] = []
ordenar (x:xs) = ordenar menor ++ [x] ++ ordenar mayor
    where
        menor = [ y | y <- xs, y < x ]
        mayor = [ y | y <- xs, y >= x ]

--------------------------------  Dado una palabra devuelve un bool en funcion de si es mayuscula o no
isUpperAll :: String -> Bool
isUpperAll [] = True
isUpperAll ")" = True
isUpperAll "." = True
isUpperAll "," = True
isUpperAll (x:xs) = ((isUpper x) || isDigit x) && isUpperAll xs
--------------------------------



devolverContenido :: [String] -> String
devolverContenido [] = ""
devolverContenido (x:xs) = x ++ " " ++ devolverContenido xs
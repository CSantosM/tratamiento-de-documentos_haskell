
module IOModule where
import System.Directory -- Necesario para leer elementos de un directorio
import Data.List
import System.IO.Unsafe
import Articulos
import HerramientasUtiles (separarLineas, listaPorPalabras, devolverContenido)

type ListadoFicheros = [String]



------------------------------------ Cargar ficheros

cargarArchivos :: IO [Articulo]
cargarArchivos = do
	contenido <- getDirectoryContents ("../recursos/ficheros")
	let fich = filter (`notElem` [".", ".."]) contenido
	leerArchivo fich
	

leerArchivo :: ListadoFicheros -> IO [Articulo]
leerArchivo [] = return crearArticuloVacio
leerArchivo (x:xs) = do
	art <- readFile ("../recursos/ficheros/" ++ x)
	let revis = head(separarLineas art)
	let ident = last (take 2(separarLineas art))
	let anio = last (take 3(separarLineas art))
	let titulo = last (take 5(separarLineas art))	
	let cont = drop 6 (separarLineas art)
	let info = CInfo (titulo ++ " (" ++ anio ++ ")") (devolverContenido(take 30(listaPorPalabras cont)) ++ "...") ((contarSecciones cont)-1) (tituloSecciones cont)

	let todos = crearArticulo revis ident anio titulo cont info
	let array = todos++(unsafePerformIO(leerArchivo xs))
	
	return array
	
	



	
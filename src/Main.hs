module Principal where
import System.IO.Unsafe
import System.Directory
import IOModule
import Articulos
import Revistas
import Acronimos

-- Cargar los ficheros de la carpeta /recursos/ficheros y lanza el men�

cargar:: IO ()
cargar = do
	let a = (unsafePerformIO(cargarArchivos))
	mostrarMenu a
		
mostrarMenu :: [Articulo] -> IO ()
mostrarMenu lista = do
	putStrLn ""
	putStrLn "////////////////////////////////////////////////////// MENU //////////////////////////////////////////////////////////"
	putStrLn "                                       Teclea una letra para elegir una opcion"
	putStrLn ""
	putStrLn "1......Obtener titulos ordenados en un anio determinado"
	putStrLn "2......Obtener el listado de revistas"
	putStrLn "3......Obtener titulos de articulos que contengan el acronimo determinado"
	putStrLn "4......Obtener titulos de articulos publicados en una revista determinada que contengan un determinado acronimo"
	putStrLn "5......Obtener listado de acronimos (y su forma expandida) de los articulos de un anio determinado"
	putStrLn "6......Mostrar el listado de acronimos que contiene un articulo y el num de apariciones dada un identificador"
	putStrLn "7......Mostrar titulos e identificadores de articulos que no contengan ningun acronimo"
	putStrLn "8......Mostrar informacion de los articulos publicados en una revista"
	putStrLn "9......Agrupar los articulos"
	putStrLn ""
	putStrLn "************************************************ Introduzca un numero ************************************************"
	opcion <- readLn
	case opcion of
		1 -> do
				putStrLn "Introduce el anio"
				a <- getLine
				(titulosOrdenados lista a)
		2 -> putStrLn(listarRevistas lista)
		3 -> do
				putStrLn "Introduce el acronimo (sin parentesis y en mayusculas)"
				acr <- getLine
				putStrLn(buscarAcronimo lista acr)
		4 -> do
				putStrLn "Introduce una revista "
				revis <- getLine
				putStrLn "Introduce un acronimo (sin parentesis y en mayusculas)"
				acr <- getLine				
				(mostrarTituloAcrRevis lista acr revis)
		5 -> do
				putStrLn "Introduce el anio de publicacion"
				anio <- getLine
				listadoAcronimos lista anio
		6 -> do
				putStrLn ("Introduce el identificador de un articulo")
				iden <- getLine
				listadoAcronimosContador lista iden
				
		7 -> mostrarSinAcronimo lista
		8 -> do 
				putStrLn "Introduce el nombre de una revista"
				revis <- getLine				
				mostrarInfo lista revis
				
		9 -> mostrarGrupos(init(init(concat(agruparArticulos lista 0))))
				
		otherwise -> return ()
	mostrarMenu lista




         
					

module Articulos (Articulo(..),Revista, ID, Year, Titulo, Contenido, Informacion(..), crearArticulo, crearArticuloVacio, contarSecciones,
 					tituloSecciones, titulosOrdenados, mostrarInfo, agruparArticulos, mostrarGrupos) where
import Data.Char
import HerramientasUtiles (ordenar)
import OrthographicMeasures



type Revista = String
type ID = String
type Year = String
type Titulo = String
type Contenido = [String]


type Resumen = String
type NumSec = Int
type Secciones = String

data Informacion = CInfo Titulo Resumen NumSec Secciones

data Grupo = CCluster ID Titulo

data Articulo = CArticulo Revista ID Year Titulo Contenido Informacion



--------------------------- Constructores

crearArticulo :: Revista -> ID -> Year -> Titulo -> Contenido -> Informacion -> [Articulo]
crearArticulo r i y t c info = [CArticulo r i y t c info]


crearArticuloVacio :: [Articulo]
crearArticuloVacio = [CArticulo "" "" "" "" [""] crearInfoVacio]
-----------

crearGrupo :: ID -> Titulo -> Grupo
crearGrupo i t = CCluster i t


crearInfo :: Titulo -> Resumen -> NumSec -> Secciones -> Informacion
crearInfo t res ns sec = CInfo t res ns sec

crearInfoVacio :: Informacion
crearInfoVacio = CInfo "" "" (-1) ""


---------------------------------------- Enunciado 1 ----------------------------------------

titulosOrdenados :: [Articulo] -> Year -> IO ()
titulosOrdenados [] _ = putStrLn "No hay titulos" 
titulosOrdenados lista anio = putStrLn (buscarTitulo (ordenar(lista)) anio)


buscarTitulo :: [Articulo] -> Year -> Titulo
buscarTitulo [] _ = ""
buscarTitulo (x:xs) anio = (buscarTituloAux x anio)++(buscarTitulo xs anio)

buscarTituloAux :: Articulo -> Year -> Titulo
buscarTituloAux (CArticulo _ _ y t c _) anio = if (anio == y) then t ++ "\n" else ""


  
 ---------------------------------------- Enunciado 8 ----------------------------------------
  
mostrarInfo :: [Articulo] -> Revista -> IO ()
mostrarInfo [] _ = return ()
mostrarInfo _ [] = return ()
mostrarInfo ((CArticulo r _ _ _ _ info):xs) revis = if (r == revis) then do
													(mostrarInfoAux info)
													(mostrarInfo xs revis)
												else (mostrarInfo xs revis)
												

mostrarInfoAux :: Informacion -> IO ()
mostrarInfoAux (CInfo tit res nsec sec) = do
	putStrLn ("Title: " ++ tit)
	putStrLn ("Abstract: " ++ res)
	putStrLn ("Section number: " ++ show nsec)
	putStrLn ("Sections: " ++ sec)
												
 
contarSecciones :: Contenido -> Int
contarSecciones [] = 0
contarSecciones (x:xs) = if (x == "-" ++ "-") then (1 + (contarSecciones xs)) else contarSecciones xs
  
tituloSecciones :: Contenido -> String
tituloSecciones [] = ""
tituloSecciones (x:xs) = if (x == "-" ++ "-") then tituloSeccionesAux (xs) ++ (tituloSecciones xs) else tituloSecciones xs

tituloSeccionesAux :: Contenido -> String
tituloSeccionesAux [] = ""
tituloSeccionesAux (x:xs) = x ++ "\n"
																																			
---------------------------------------- Enunciado 9 ----------------------------------------

agruparArticulos :: [Articulo] -> Int -> [[Grupo]]
agruparArticulos [] _ = [[]]
agruparArticulos ((CArticulo r i y t c _):xs) num = do
												let cont = cont+1
												let g1 = crearGrupo i t
												let grupo = [crearGrupo ("Grupo: "++ show num) ""]++ [g1] ++ (aceptados g1 xs)
												let resto = rechazados g1 xs
												let g2 = [grupo] ++ agruparArticulos resto	(succ num)						
												g2
												
												
											
											
aceptados :: Grupo -> [Articulo] -> [Grupo]
aceptados _ [] = []
aceptados (CCluster i t) ((CArticulo _ i2 _ t2 c2 _):xs) = if (similars t t2 25) then
																[(crearGrupo i2 t2)] ++ (aceptados (crearGrupo i t) xs)
															else
																(aceptados (crearGrupo i t) xs)
																
																
																
rechazados :: Grupo -> [Articulo] -> [Articulo]
rechazados _ [] = []
rechazados (CCluster i t) ((CArticulo r2 i2 y2 t2 c2 info):xs) = if not(similars t t2 25) then
																(crearArticulo r2 i2 y2 t2 c2 info) ++ (rechazados (crearGrupo i t) xs)
															else 
																(rechazados (crearGrupo i t) xs)
												
														
mostrarGrupos :: [Grupo] -> IO()
mostrarGrupos [] = return ()
mostrarGrupos ((CCluster i t):xs) = do 	
												putStrLn (i ++ " .- " ++ t)
												mostrarGrupos xs
										
------------------------------------------------------------------------------------------------------------												

instance Ord Articulo where 
	CArticulo r i y t c info <= CArticulo r2 i2 y2 t2 c2 info2 = (t<t2) || (t==t2 && y>y2)
	CArticulo r i y t c info > CArticulo r2 i2 y2 t2 c2 info2 = (t>t2) || (t==t2 && y<y2)
	
instance Eq Articulo where 
	CArticulo r i y t c info == CArticulo r2 i2 y2 t2 c2 info2 = ((r==r2)&&(i==i2)&&(y==y2)&&(t == t2)&&(c==c2))
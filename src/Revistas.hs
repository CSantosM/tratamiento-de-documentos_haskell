
module Revistas(listarRevistas) where
import Articulos (Articulo(..),Revista)
import HerramientasUtiles (devolverContenido)
import Data.List

type NombreRevistas = [String]

---------------------------------------- Enunciado 2 ----------------------------------------
listarRevistas :: [Articulo] -> Revista
listarRevistas [] = []
listarRevistas lista = devolverContenido (nub (conjuntoRevistas lista)) -- uso nub para eliminar elementos repetidos de la lista

conjuntoRevistas :: [Articulo] -> NombreRevistas
conjuntoRevistas [] = []
conjuntoRevistas (x:xs) = ["Listado de revistas: \n"] ++ (getRevista x ++ "\n"):(conjuntoRevistas xs)

getRevista :: Articulo -> Revista
getRevista (CArticulo "" "" "" "" [""] crearInfoVacio) = ""
getRevista (CArticulo r _ _ _ _ _) = r

  